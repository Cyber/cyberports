### CyberPorts ###

Ports created for personal use, use at your own risk, corrections and recommendations welcome

Current Ports:
 -  w3m - Because w3m from main repository didn't work for me
 -  libstrophe
 -  profanity
 -  leveldb
 -  minetest (5.8.0)
 -  st - fork of the spkgbuild st with ttf-liberation as a dependency because it is the program default
 -  libcurl-gnutls - fork of the spkgbuild libcurl-gnutls with libpsl package as dependency, otherwise compilation will fail
